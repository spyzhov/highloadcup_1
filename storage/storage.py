from multiprocessing.managers import BaseManager


class Storage(object):
    def __init__(self):
        self._storage = {
            'locations': {},
            'users': {},
            'visits': {},
        }
        self._index = {
            'user_visit': {},
            'location_visit': {},
        }

    def count(self, name):
        return len(self._storage[name])

    def save(self, name, entity):
        self._storage[name][entity.get('id')] = entity
        self._after_save(name, entity)

    def get(self, name, id):
        return self._storage[name].get(id, None)

    def user_visit(self, user_id):
        user_visits = self._index['user_visit'].get(user_id, None)
        if isinstance(user_visits, dict):
            return user_visits.values()
        return None

    def location_visit(self, location_id):
        location_visits = self._index['location_visit'].get(location_id, None)
        if isinstance(location_visits, dict):
            return location_visits.values()
        return None

    def create(self, name, new):
        if None in new.values() or new.get('id') in self._storage[name]:
            raise ValueError
        self.save(name, new)

    def update(self, name, id, new):
        old = self.get(name, id)
        if old is None:
            raise KeyError
        if None in new.values():
            raise ValueError
        self._before_update(name, old)
        old.update(new)
        self._after_save(name, old)

    def _after_save(self, name, entity):
        if name == 'users':
            self._index['user_visit'].setdefault(entity.get('id'), {})
        if name == 'locations':
            self._index['location_visit'].setdefault(entity.get('id'), {})
        if name == 'visits':
            self._index_append('user_visit', entity.get('user'), entity.get('id'), {
                'visit': entity,
                'location': self.get('locations', entity.get('location'))
            })
            self._index_append('location_visit', entity.get('location'), entity.get('id'), {
                'mark': entity.get('mark'),
                'visited_at': entity.get('visited_at'),
                'user': self.get('users', entity.get('user')),
            })

    def _before_update(self, name, old):
        if name == 'visits':
            self._index_remove('user_visit', old.get('user'), old.get('id'))
            self._index_remove('location_visit', old.get('location'), old.get('id'))

    def _index_append(self, index, index_id, object_id, object):
        self._index[index].setdefault(index_id, {})
        self._index[index][index_id][object_id] = object

    def _index_remove(self, index, index_id, object_id):
        try:
            self._index[index][index_id].pop(object_id)
        except KeyError:
            pass


class StorageManager(BaseManager):
    pass


def Manager():
    StorageManager.register('Storage', Storage)
    man = StorageManager()
    man.start()
    return man
