from .storage import Storage, Manager
import json
import os
from datetime import datetime


objects = {
    'Storage': None
}


def prepare(zip_file, path):
    if os.path.isfile(zip_file):
        import zipfile
        zip_ref = zipfile.ZipFile(zip_file, 'r')
        zip_ref.extractall(path)
        zip_ref.close()


def load_path(path):
    files = sorted([path + name for name in os.listdir(path) if name != 'options.txt' and name.endswith('.json')])
    manager = Manager()
    objects['Storage'] = manager.Storage()
    index, total = 0, len(files)
    for filename in files:
        print("%d/%d \t%s" % (index + 1, total, os.path.basename(filename)))
        _load(filename)
        index += 1
    return objects['Storage']


def _load(filename):
    with open(filename, 'r') as handler:
        data = json.load(handler)
        for name in data:
            for entity in data.get(name):
                objects['Storage'].save(name, entity)


def get_now(options):
    for filename in options:
        if os.path.isfile(filename):
            with open(filename, 'r') as h:
                return datetime.fromtimestamp(int(h.readline()))
    print('options not found')
    return datetime.now()
