FROM python:3.5.4-slim
WORKDIR /root
RUN mkdir data/
#COPY data /root/data
COPY storage /root/storage
COPY requirements.txt /root/
COPY app.py /root/

RUN pip3 install -r requirements.txt

CMD [ "python3", "./app.py" ]
EXPOSE 80