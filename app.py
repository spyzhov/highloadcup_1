#!/usr/bin/python
# -*- coding: utf-8 -*-
import tornado.ioloop
import tornado.web
# import tornado.httpserver
import tornado.escape
from tornado.options import define, options, parse_command_line
from datetime import datetime
import calendar
from storage import prepare, load_path, get_now
import time
import copy


# https://highloadcup.ru/
# TODO: add cache?

define("port", default=80, help="run on the given port", type=int)
define("debug", default=False, help="run in debug mode")

FILE = '/tmp/data/data.zip'
PATH = 'data/'
OPTIONS = ['data/options.txt', '/tmp/data/options.txt']

CONFIG = {
    'now': datetime.now()
}


class RestHandler(tornado.web.RequestHandler):
    def initialize(self, storage):
        self.storage = storage

    def get(self, entity, id):
        obj = self.storage.get(
            tornado.escape.to_basestring(entity),
            int(id)
        )
        if isinstance(obj, dict):
            self.write(obj)
        else:
            self.set_status(404)  # raise tornado.web.HTTPError(404)
            self.finish()

    def post(self, entity, id):
        try:
            self.storage.update(
                tornado.escape.to_basestring(entity),
                int(id),
                tornado.escape.json_decode(self.request.body)
            )
            self.write({})
        except ValueError:
            self.set_status(400)
        except KeyError:
            self.set_status(404)
        finally:
            self.finish()


class NewHandler(tornado.web.RequestHandler):
    def initialize(self, storage):
        self.storage = storage

    def post(self, entity):
        try:
            self.storage.create(
                tornado.escape.to_basestring(entity),
                tornado.escape.json_decode(self.request.body)
            )
            self.write({})
        except ValueError:
            self.set_status(400)
        except KeyError:
            self.set_status(404)
        finally:
            self.finish()


class VisitsHandler(tornado.web.RequestHandler):
    def initialize(self, storage):
        self.storage = storage

    def get(self, user_id):
        user_visits = self.storage.user_visit(int(user_id))
        if user_visits is None:
            self.set_status(404)
            self.finish()
            return

        from_date = self.get_argument("fromDate", None)
        to_date = self.get_argument("toDate", None)
        country = self.get_argument("country", None)
        to_distance = self.get_argument("toDistance", None)

        try:
            if from_date is not None:
                from_date = int(from_date)
            if to_date is not None:
                to_date = int(to_date)
            if to_distance is not None:
                to_distance = int(to_distance)
            if country is not None:  # research
                country = tornado.escape.to_basestring(country)
                if len(country) == 0:
                    raise ValueError
        except ValueError:
            self.set_status(400)
            self.finish()
            return

        result = []
        for user_visit in user_visits:
            visit = user_visit.get('visit')
            location = user_visit.get('location')
            visited_at = visit.get('visited_at')

            if from_date is not None and visited_at <= from_date:
                continue
            if to_date is not None and visited_at >= to_date:
                continue
            if to_distance is not None and location.get('distance') >= to_distance:
                continue
            if country is not None and country != location.get('country'):
                continue

            result.append({
                    "mark": visit.get('mark'),
                    "visited_at": visited_at,
                    "place": location.get('place'),
                })

        self.write({
            'visits': sorted(result, key=lambda a: a.get('visited_at'))
        })


class LocationAvgHandler(tornado.web.RequestHandler):
    def initialize(self, storage):
        self.storage = storage

    def get(self, location_id):
        location_visits = self.storage.location_visit(int(location_id))
        if location_visits is None:
            self.set_status(404)
            self.finish()
            return

        from_date = self.get_argument("fromDate", None)
        to_date = self.get_argument("toDate", None)
        from_age = self.get_argument("fromAge", None)
        to_age = self.get_argument("toAge", None)
        gender = self.get_argument("gender", None)

        try:
            if from_date is not None:
                from_date = int(from_date)
            if to_date is not None:
                to_date = int(to_date)
            if from_age is not None:
                from_age = calendar.timegm(years_ago(int(from_age)).timetuple())
            if to_age is not None:
                to_age = calendar.timegm(years_ago(int(to_age)).timetuple())
            if gender is not None:
                gender = tornado.escape.to_basestring(gender)
                if not(gender in ['f', 'm']):
                    raise ValueError
        except ValueError:
            self.set_status(400)
            self.finish()
            return

        result = []
        for location_visit in location_visits:
            if from_date is not None or to_date is not None:
                visited_at = location_visit.get('visited_at')
                if from_date is not None and visited_at <= from_date:
                    continue
                if to_date is not None and visited_at >= to_date:
                    continue
            if from_age is not None or to_age is not None or gender is not None:
                user = location_visit.get('user')
                birth_date = user.get('birth_date')
                if from_age is not None and birth_date >= from_age:
                    continue
                if to_age is not None and birth_date <= to_age:
                    continue
                if gender is not None and gender != user.get('gender'):
                    continue
            result.append(location_visit.get('mark'))

        self.write({
            'avg': round(float(sum(result)) / len(result) if len(result) > 0 else 0, 5)
        })


def years_ago(years, from_date=None):
    """ used: https://stackoverflow.com/a/765990 """
    if from_date is None:
        from_date = copy.copy(CONFIG['now'])
    try:
        return from_date.replace(year=from_date.year - years)
    except ValueError:
        # Must be 2/29!
        assert from_date.month == 2 and from_date.day == 29  # can be removed
        return from_date.replace(month=2, day=28, year=from_date.year-years)


def main():
    parse_command_line()
    t_start = time.time()
    print('Loading...')
    prepare(FILE, PATH)
    storage = load_path(PATH)
    print('Loaded %0.5f' % (time.time() - t_start))
    for k in ["locations", "users", "visits"]:
        print('\t%s: %d' % (k, storage.count(k)))
    CONFIG['now'] = get_now(OPTIONS)

    app = tornado.web.Application(
        [
            (r"/(locations|users|visits)/(\d+)", RestHandler, dict(storage = storage)),
            (r"/(locations|users|visits)/new", NewHandler, dict(storage = storage)),
            (r"/users/(\d+)/visits", VisitsHandler, dict(storage = storage)),
            (r"/locations/(\d+)/avg", LocationAvgHandler, dict(storage = storage)),
        ],
        debug=options.debug,
    )
    app.listen(options.port)
    # server = tornado.httpserver.HTTPServer(app)
    # server.bind(options.port)
    # server.start(0)
    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
